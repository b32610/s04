import com.zuitt.example.*;

public class Main {

    public static void main(String[] args) {

        // System.out.println("Hello world!");

        Car myCar = new Car();
        myCar.drive();

        // setter
        myCar.setName("Toyota");
        myCar.setBrand("Vios");
        myCar.setYearOfMake(2015);
        myCar.setDriverName("John Smith");

        Car myCar2 = new Car("Kia", "Sorrento", 2022);

        // getter
        System.out.println(myCar.getname());
        System.out.println(myCar.getBrand());
        System.out.println(myCar.getYearOfMake());
        System.out.println(myCar.getDriverName());

        // inheritance
        // Dog is animal (Dog inherits animal class)

        Dog myPet = new Dog();
        myPet.setName("Izumi");
        myPet.setColor("White");
        myPet.speak();
        myPet.call();
        System.out.println(myPet.getName() + " " + myPet.getColor() + " " + myPet.getBreed() );

        // Person implements aactions and greetings
        Person child = new Person();

        child.sleep();
        child.run();
        child.holidayGreet();
        child.morningGreet();

        // static or compile time polymorphism

        StaticPoly myAddition = new StaticPoly();
        System.out.println(myAddition.addition(5, 4));
        System.out.println(myAddition.addition(5,4,6));
        System.out.println(myAddition.addition(5.5,4.4));

        // Dynamic or run-time polymorphism
        // Function is overridden by replacing the definition of the method in the parent class in the child class.

        Child myChild = new Child();
        myChild.speak();



    }

    /*
        OBJECT ORIENTED PROGRAMMING

            Objective
               Define what are classes and objects
               Define and distinguish the 4 pillars of OOP
               Create classes that implement encapsulation and inheritance

               OBJECTS - an abstract idea in your mind that represents something in the real world
                        "concept of the dog"
               CLASS = THe representation of the object in code
                        "what the dog does"
               INSTANCE = a unique copy of the idea, made "tangible"
                        "dog with instantiation, assigned attributes"



               Encapsulation - is a mechanism wrapping around

               variables of a class will be hidden from other classes, can access only though the getters and setters. Also known as data hiding

               Encapsulation is achieved by declaring variables of classes as private and by adding getters and setters.



               Inheritance = can be defined as the process where one class acquires the properties ( methods and fields) of another class.

               With the use of inheritance the information is made manageable in a hierarchical order

               4 PILLARS OF OOP
               Encapsulation
               Inheritance
               Polymorphism
               Abstraction

               >>> Encapsulation
                    a mechanism of wrapping the data (variables) and code acting on the data (methods) together as a single unit

                    "data hiding" -  the variables of a class will be hidden from other classes, and can be accessed only through the methods of their current class.

                To achieve encapsulation:
                    variables/properties as private.
                    provide a public setter and getter function.

                 ++++ Composition and Inheritance +++++

                 Both concepts promotes code reuse through different approach
                 "Inheritance" allows modelling an object that is a subset of another objects.
                     It defines “is a relationship”.
                     To design a class on what it is.

                 "Composition" allows modelling objects that are made up of other objects.
                     both entities are dependent on each other
                     composed object cannot exist without the other entity.
                     It defines “has a relationship”.
                     To design a class on what it does.

                 Example:
                     A car is a vehicle - inheritance
                     A car has a driver - composition

              >>> Inheritance - Can be defined as the process where one class acquires the properties and methods of another class.

               With the use of inheritance, the information is made manageable in hierarchical order.

             >> Abstraction -- it is a process where all the logic and complexity are hidden from the user.

                // Interfaces
                    // This is used to achieve total abstraction/
                    // Creating abstract classes doesn't support "multiple inheritance", but it can be achieved with interfaces.
                    // act as "contracts" where in a class implements the interface should have the methods that the interfaces has defined in the class.

                     Interface vs Abstract

                Abstract is limited to 1 inheritance
                Interface can inherit multiple implementations



             imagine a tv remote where the users are only familiar on what to do on how to operate it.

             Most users don't know how they work on the inside and it isn't necessary for them to know which may cause confusion if included in the manual

             >> Polymorphism - ability to take on many forms
                    Derived from greek word : poly means many and morph means forms
                        In short "many forms'

                     // this is usually done by function/methods overriding/overloading

                     // two main types of polymorphiosm
                     // static or compile time polymorphism
                     m

             Several object have the same function names but perform different functionalities based on their used


                OOP stands for "Object-Oriented Programming".
                OOP is a programming model that allows developers to design software around data or objects, rather than function and logic.

                OOP Concepts

                 Object - abstract idea that represents something in the real world.
                 Example: The concept of a dog

                 Class - representation of the object using code.
                 Example: Writing a code that would describe a dog.

                 Instance - unique copy of the idea, made "physical".
                 Example: Instantiating a dog names Spot from the dog class.
                 Objects

                 States and Attributes-what is the idea about?
                 Behaviors-what can idea do?

                 Example: A person has attributes like name, age, height, and weight. And a person can eat, sleep, and speak.






    */





}