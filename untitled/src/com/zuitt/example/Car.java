package com.zuitt.example;

public class Car {
    // private - access modifier
        // These are used to restrict the scope of a class constructor, variable, method or data member.
//    four types of access modifiers :
    /*
        1. Default - no keyword indicated (accessibility is within the package)
        2. Private - Properties or method only accessible within the class.
        3. Protected - properties and methods are only accessible by the class of the same package and the subclass present in any package.
        4. Public - properties and methods can be accessed from anywhere.


    */
    // Class creation
    // Four parts of the class creation

    /*
        1 Properties - characteristics of an object.
    */

    private String name;
    private String brand;
    private int yearOfMake;

    // Make a component of a car
    private  Driver driver;

    /*
        2 Constructors - used to create/instantiate an object :
    */

    // empty constructor - creates object that doesn't have any arguments or parameters
        public Car(){
                this.driver = new Driver("Alejandro");
        }


    // parameterized constructor - creates an object with argument/parameters
    public Car(String name, String brand, int yearOfMake ){
        this.name = name;
        this.brand = brand;
        this.yearOfMake = yearOfMake;
        this.driver = new Driver("alejandro");
    }


    // 3 Getters and Setters - get and set the values of each property of the object.
    // Getter = used to retrieve the value of an instantiated object.
    public String getname(){
        return this.name;
    }

    public String getBrand(){
        return this.brand;
    }

    public int getYearOfMake(){
        return this.yearOfMake;
    }

    public String getDriverName(){
        return this.driver.getName();
    }

    // Setter = used to change he default values of an instantiated object
    public void  setName(String name){
        this.name = name;
    }
    public void setBrand(String brand){
        this.brand = brand;
    }

    public void setYearOfMake(int yearOfMake){
        if(yearOfMake < 2022){

        this.yearOfMake = yearOfMake;
        }
    }

    public void setDriverName(String driver){
        this.driver.setName(driver);
    }



    // 4. Methods -  functions of an object it can perform (action)
    public void drive(){
        System.out.println("im driving");
    }


}
