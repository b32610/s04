package com.zuitt.example;
// interfaces implementation
public class Person implements Actions, Greetings {

     public void sleep(){
         System.out.println("zzz..");
     }
     public void run(){
         System.out.println("runrun");
     }

    @Override
    public void holidayGreet() {
        System.out.println("Happy Holidays");
    }

    @Override
    public void morningGreet() {
        System.out.println("Good morning!");
    }
}


