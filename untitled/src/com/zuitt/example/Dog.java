package com.zuitt.example;

// Child class of animal class
    // "extend" keyword used to inherit the properties of the parent class.

public class Dog extends Animal {
    // properties
    private String breed;

    // constructor
    public Dog(){
        // This will inherit the Animal() constructor.
        super();
        this.breed = "Chihuahua";
    }

    public Dog(String name, String color, String breed){
        super(name, color);
        this.breed = breed;
    }

    // getter and setter
    public String getBreed(){
        return this.breed;
    }

    public void setBreed(String breed){
        this.breed = breed;
    }

    // Method
    public void speak(){
        System.out.println("Woof Woof");
    }
}
