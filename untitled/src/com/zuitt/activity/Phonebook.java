package com.zuitt.activity;

import java.util.ArrayList;

public class Phonebook {
    //STEP 6
    private ArrayList<Contact> contacts = new ArrayList<>();

    //STEP 7
    public Phonebook(){}

    public Phonebook(Contact contacts){
        this.contacts.add(contacts);
    }

    //STEP 8
    //Getter
    public ArrayList<Contact> getContacts(){ return this.contacts; }

    //Setter
    public void setContacts(Contact contacts){ this.contacts.add(contacts); }
}

