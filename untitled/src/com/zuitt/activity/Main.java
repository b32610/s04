package com.zuitt.activity;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        Phonebook phonebook = new Phonebook();


        Contact Denise = new Contact();
        Denise.setName("Denise Bernas");
        Denise.setContactNumber("+639152468596");
        Denise.setAddress("SW");

        Contact Jherson = new Contact();
        Jherson.setName("Jherson Dignadice");
        Jherson.setContactNumber("+639162148573");
        Jherson.setAddress("jherson");


        phonebook.setContacts(Denise);
        phonebook.setContacts(Jherson);


        if (phonebook.getContacts().size() == 0) {
            System.out.println("Phonebook is empty");
        } else {
            phonebook.getContacts().forEach(contact -> {
                System.out.println("+++++++++++++++++++++++++++");
                System.out.println(contact.name);
                System.out.println("+++++++++++++++++++++++++++");
                System.out.println(contact.name + " has the following registered number:");
                System.out.println(contact.contactNumber);
                System.out.println(contact.name + " has the following registered address:");
                System.out.println("My home in " + contact.address);
            });
        }
    }
}
